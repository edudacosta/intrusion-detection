import torch
from torchvision.transforms import Compose, Normalize, ToTensor

import pandas
import json
import math
from tqdm import tqdm
import os
import time

from data import  get_dataloader, normalize, preproc_df, split_dataset
from models import AutoEncoder, MLP
from utils import scores

from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_recall_curve

def test(dataloader, cfg):
    cfg = cfg["mlp"]

    model = torch.load(cfg["test_model"])
    autoencoder = torch.load(cfg["autoencoder"])

    if cfg["loss_fn"] == "cross_entropy":
        loss_fn = torch.nn.BCELoss()

    model.eval()
    autoencoder.eval()

    thresholds = range(0, 2)
    th_step = 0.5

    tpr_vs_fpr = pandas.DataFrame(columns=['FPR', 'TPR'])

    tprs = list()
    frps = list()

    recalls = list()
    precisions = list()

    ap = 0

    inf_time = 0

    for data in dataloader:
        inputs = data["input"]
        labels = data["label"].view(-1,1)

        with torch.no_grad():
            encoded = autoencoder.encoder(inputs)
            time1 = time.time()
            outputs = model(encoded)
            inf_time += (time.time() - time1) / inputs.shape[0]

            loss = loss_fn(outputs, labels)

            for th in thresholds:
                predicted = (outputs.data >= th*th_step).long()

                acc = (predicted  == labels).sum().item() / labels.shape[0]

                ap += average_precision_score(labels.cpu().detach().numpy(),
                    outputs.cpu().detach().numpy())

                precision, recall, fpr = scores(predicted, labels)
                print(precision.item(), recall.item(), fpr.item(), acc)
                tprs.append(recall.item())
                frps.append(fpr.item())

                new_lab = labels.cpu().detach().numpy().reshape(-1)
                new_pred = outputs.cpu().detach().numpy().reshape(-1)
                precision, recall, _ = precision_recall_curve(new_lab, new_pred)

                precisions = list(precision)
                recalls = list(recall)

                # recalls.append(recall.item())
                # precisions.append(precision.item())


    print('mean time', inf_time/len(dataloader))

    tpr_vs_fpr = pandas.DataFrame({'FPR':frps, 'TPR':tprs})
    recall_vs_precision = pandas.DataFrame({'Recall': recalls, 'Precision': precisions})

    tpr_vs_fpr.to_csv('results/MLP_TPR_vs_FPR.csv')
    recall_vs_precision.to_csv('results/MLP_RECALL_vs_PRECISION.csv')

    print("mAP: ",  ap/1001)

def main():
    with open('config.json', 'r') as f:
        cfg = json.load(f)

    test_dataframe = pandas.read_csv(cfg["test_data"], header=0, names=cfg['attributes'].keys())

    test_dataframe = preproc_df(test_dataframe, cfg)
    test_dataframe = normalize(test_dataframe)

    test_dataloader = get_dataloader(test_dataframe, batch_size=test_dataframe.shape[0])

    test(test_dataloader, cfg)


if __name__=='__main__':
    main()