import torch
from torchvision.transforms import Compose, Normalize, ToTensor
from torch.utils.tensorboard import SummaryWriter

import pandas
import json
import math
from tqdm import tqdm
import os
import time

from data import  get_dataloader, normalize, preproc_df, split_dataset
from models import LSTM
from utils import scores

from torch.nn.utils.rnn import pack_sequence


def train_lstm(dataloader, val_dataloader, cfg):
    cfg = cfg["lstm"]
    epochs = cfg["epochs"]
    nb = len(dataloader) # number of batchs
    v_nb = len(val_dataloader)
    logdir = cfg["log_dir"]

    if os.path.isdir(logdir):
        exp_n = len(os.listdir(logdir)) + 1
    else:
        os.makedirs(logdir)
        exp_n = 1

    exp_dir = os.path.join(logdir, ("exp%d"  % exp_n))
    os.makedirs(exp_dir)

    writer = SummaryWriter(log_dir=exp_dir, comment=f"LSTM")
    
    # train setup
    # model
    model = LSTM()

    # loss function
    if cfg["loss_fn"] == "cross_entropy":
        loss_fn = torch.nn.BCELoss()

    # optmizer
    if cfg["optmizer"] == "adam":
        optim = torch.optim.Adam(model.parameters(), lr=cfg['lr'])
    elif cfg["optmizer"] == "sgd":
        optim = torch.optim.SGD(model.parameters(), lr=cfg['lr'], momentum=cfg["momentum"])

    writer.add_text('hparams', 
        f"opt: {cfg['optmizer']} - lr: {cfg['lr']} - batch: {cfg['batch_size']} - momentum: {cfg['momentum']}")

    since = time.time()

    for epoch in range(epochs):
        epoch_loss = 0.0
        epoch_acc = 0.0
        epoch_prec = 0.0
        epoch_rec = 0.0
        val_epoch_acc = 0

        val_epoch_loss = 0.0
        best_loss = math.inf

        model.train()
        
        t_dataloader = tqdm(dataloader, desc=f"Epoch (train): {epoch}/{epochs}")
        for i, data in enumerate(t_dataloader):
            inputs = pack_sequence([data["input"]])
            labels = data["label"]

            labels = labels.view(-1,1)

            optim.zero_grad()

            outputs = model(inputs)

            loss = loss_fn(outputs, labels)

            loss.backward()
            optim.step()
            predicted = (outputs.data >= 0.5).long()

            acc = (predicted  == labels).sum().item() / labels.shape[0]
            
            epoch_loss += loss.item()
            epoch_acc += acc
            t_dataloader.set_description(
                f"Epoch (train): {epoch}/{epochs}, loss: {epoch_loss/(i+1):.5f}, acc: {epoch_acc/(i+1):.2f}")

        model.eval()

        v_dataloader = tqdm(val_dataloader, desc=f"Epoch (val): {epoch}/{epochs}")
        for i, data in enumerate(v_dataloader):
            val_inputs = pack_sequence([data["input"]])
            labels = data["label"]
            labels = labels.view(-1,1)

            with torch.no_grad():
                val_outputs = model(val_inputs)
                val_loss = loss_fn(val_outputs, labels)
                val_epoch_loss += val_loss.item()
                predicted = (val_outputs.data >= 0.5).long()
                
                val_acc = (predicted  == labels).sum().item() / labels.shape[0]

                precision, recall, _ = scores(predicted, labels)

                val_epoch_acc += val_acc
                epoch_prec += precision
                epoch_rec += recall

                v_dataloader.set_description(
                    f"Epoch (val): {epoch}/{epochs}, loss: {val_epoch_loss/(i+1):.5f}, acc: {val_epoch_acc/(i+1):.2f}")

        epoch_loss /= nb
        epoch_acc /= nb
        val_epoch_loss /= v_nb
        val_epoch_acc /= v_nb
        epoch_prec /= v_nb
        epoch_rec /= v_nb

        writer.add_scalars('Accuracy', {'train': epoch_acc, 'val':val_epoch_acc}, epoch)
        writer.add_scalars('Loss', {'train': epoch_loss, 'val':val_epoch_loss}, epoch)
        writer.add_scalar('precision', epoch_prec, epoch)
        writer.add_scalar('recall', epoch_rec, epoch)

        if val_epoch_loss < best_loss:
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optim.state_dict(),
                'loss': epoch_loss
            }, os.path.join(exp_dir, "best_dict.pt"))

            torch.save(model, os.path.join(exp_dir, "best.pt"))
            best_loss = val_epoch_loss

        if epoch % 10 == 0:
            torch.save(model, os.path.join(exp_dir, f"model_epoch_{epoch:03d}.pt"))

        train_time = time.time() - since
        writer.add_scalar('Time/train', train_time, epoch)

    writer.close()


def main():
    with open('config.json', 'r') as f:
        cfg = json.load(f)

    train_dataframe = pandas.read_csv(cfg["train_data"], header=0, names=cfg['attributes'].keys())
    val_dataframe = pandas.read_csv(cfg["val_data"], header=0, names=cfg['attributes'].keys())

    train_dataloader = get_dataloader(train_dataframe, batch_size=cfg["lstm"]["batch_size"])
    val_dataloader = get_dataloader(val_dataframe, batch_size=cfg["lstm"]["batch_size"])


    train_lstm(train_dataloader, val_dataloader, cfg)

if __name__=='__main__':
    main()