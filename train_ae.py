import torch
from torchvision.transforms import Compose, Normalize, ToTensor
from torch.utils.tensorboard import SummaryWriter

import pandas
import json
import math
from tqdm import tqdm
import os
import time

from data import  get_dataloader, normalize, preproc_df, split_dataset
from models import AutoEncoder

def train_autoencoder(dataloader, val_dataloader, cfg):
    cfg = cfg["autoencoder"]
    epochs = cfg["epochs"]
    nb = len(dataloader) # number of batchs
    v_nb = len(val_dataloader)
    logdir = cfg["log_dir"]

    if os.path.isdir(logdir):
        exp_n = len(os.listdir(logdir)) + 1
    else:
        os.makedirs(logdir)
        exp_n = 1

    exp_dir = os.path.join(logdir, ("exp%d"  % exp_n))
    os.makedirs(exp_dir)

    writer = SummaryWriter(log_dir=exp_dir, comment=f"AUTOENCODER")
    
    # train setup
    # model
    model = AutoEncoder()

    # loss function
    if cfg["loss_fn"] == "mse":
        loss_fn = torch.nn.MSELoss()

    # optmizer
    if cfg["optmizer"] == "adam":
        optim = torch.optim.Adam(model.parameters(), lr=cfg['lr'])
    elif cfg["optmizer"] == "sgd":
        optim = torch.optim.SGD(model.parameters(), lr=cfg['lr'], momentum=cfg["momentum"])

    writer.add_text('hparams', 
        f"opt: {cfg['optmizer']} - lr: {cfg['lr']} - batch: {cfg['batch_size']} - momentum: {cfg['momentum']}")

    since = time.time()

    for epoch in range(epochs):
        epoch_loss = 0.0
        val_epoch_loss = 0.0
        best_loss = math.inf

        model.train()
        
        t_dataloader = tqdm(dataloader, desc=f"Epoch (train): {epoch}/{epochs}")
        for i, data in enumerate(t_dataloader):
            inputs = data["input"]

            optim.zero_grad()

            outputs = model(inputs)

            loss = loss_fn(outputs, inputs)
            loss.backward()
            optim.step()

            epoch_loss += loss.item()
            t_dataloader.set_description(f"Epoch (train): {epoch}/{epochs}, loss: {epoch_loss/(i+1):.5f}")

        model.eval()

        v_dataloader = tqdm(val_dataloader, desc=f"Epoch (val): {epoch}/{epochs}")
        for i, data in enumerate(v_dataloader):
            val_inputs = data["input"]
            with torch.no_grad():
                val_outputs = model(data["input"])
                val_loss = loss_fn(val_outputs, val_inputs)
                val_epoch_loss += val_loss.item()
                v_dataloader.set_description(f"Epoch (val): {epoch}/{epochs}, loss: {val_epoch_loss/(i+1):.5f}")

        epoch_loss /= nb
        val_epoch_loss /= v_nb
        writer.add_scalars('Loss', {'train': epoch_loss, 'val':val_epoch_loss}, epoch)
        # writer.add_scalar('Loss/train', epoch_loss, epoch)
        # writer.add_scalar('Loss/val', val_epoch_loss, epoch)

        if val_epoch_loss < best_loss:
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optim.state_dict(),
                'loss': epoch_loss
            }, os.path.join(exp_dir, "best_dict.pt"))

            torch.save(model, os.path.join(exp_dir, "best.pt"))
            best_loss = val_epoch_loss

        if epoch % 10 == 0:
            torch.save(model, os.path.join(exp_dir, f"model_epoch_{epoch:03d}.pt"))

        train_time = time.time() - since
        writer.add_scalar('Time/train', train_time, epoch)

    writer.close()


def main():
    
    with open('config.json', 'r') as f:
        cfg = json.load(f)



    train_dataframe = pandas.read_csv(cfg["train_data"], header=0, names=cfg['attributes'].keys())
    val_dataframe = pandas.read_csv(cfg["val_data"], header=0, names=cfg['attributes'].keys())


    dataframe = pandas.read_csv('KDD/KDDTrain+.txt', names=cfg['attributes'].keys())

    print(dataframe.max())

    return

    # train_dataframe = preproc_df(train_dataframe,cfg)
    # train_dataframe = normalize(train_dataframe)
    # train_dataframe, val_dataframe = split_dataset(train_dataframe, validation_split=0.3)

    train_dataloader = get_dataloader(train_dataframe, batch_size=cfg["autoencoder"]["batch_size"])
    val_dataloader = get_dataloader(val_dataframe, batch_size=cfg["autoencoder"]["batch_size"])


    train_autoencoder(train_dataloader, val_dataloader, cfg)


if __name__=='__main__':
    main()