import torch
import pandas
import json
from data import normalize, preproc_df, get_dataloader
from torch.nn.utils.rnn import pack_sequence
import numpy as np

from utils import scores

from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_recall_curve

import time

def test(test_dataloader, cfg):
    cfg = cfg["lstm"]

    model = torch.load(cfg["test_model"])

    if cfg["loss_fn"] == "cross_entropy":
        loss_fn = torch.nn.BCELoss()

    thresholds = range(0, 1001)
    th_step = 0.0001

    tprs = list()
    frps = list()

    recalls = list()
    precisions = list()

    ap = 0

    inf_time = 0

    for data in test_dataloader:
        with torch.no_grad():
            inputs = pack_sequence([data["input"]])
            labels = data["label"].view(-1, 1)
            time1 = time.time()
            outputs = model(inputs)
            inf_time += (time.time() - time1) / inputs.data.shape[0]

            loss = loss_fn(outputs, labels)
            for th in thresholds:
                predicted =  (outputs.data > th*th_step).long()

                acc = (predicted  == labels).sum().item() / labels.shape[0]
                ap += average_precision_score(labels.cpu().detach().numpy(),
                    outputs.cpu().detach().numpy())

                precision, recall, fpr = scores(predicted, labels)
                # print(precision.item(), recall.item(), fpr.item(), acc)
                tprs.append(recall.item())
                frps.append(fpr.item())

                recalls.append(recall.item())
                precisions.append(precision.item())

    print('mean time', inf_time/len(test_dataloader))

    tpr_vs_fpr = pandas.DataFrame({'FPR':frps, 'TPR':tprs})

    recall_vs_precision = pandas.DataFrame({'Recall': recalls, 'Precision': precisions})

    tpr_vs_fpr.to_csv('results/LSTM_TPR_vs_FPR.csv')
    recall_vs_precision.to_csv('results/LSTM_RECALL_vs_PRECISION.csv')

    print("mAP: ",  ap/1001)

def main():

    with open('config.json', 'r') as f:
        cfg = json.load(f)
    
    #prepare dataset to test:
    test_path = cfg["test_data"]
    test_dataframe = pandas.read_csv(test_path, header=0, names=cfg['attributes'].keys())
    test_dataframe = preproc_df(test_dataframe,cfg)
    test_dataframe = normalize(test_dataframe)
    test_dataloader = get_dataloader(test_dataframe,test_dataframe.shape[0])
    
    test(test_dataloader, cfg)





    




if __name__=='__main__':
    main()