import pandas

import matplotlib.pyplot as plt 


def main():
    df1 = pandas.read_csv("results/MLP_TPR_vs_FPR.csv")
    df2 = pandas.read_csv("results/LSTM_TPR_vs_FPR.csv")

    x = df1.iloc[:,1].values
    y = df1.iloc[:,2].values

    x2 = df2.iloc[:,1].values
    y2 = df2.iloc[:,2].values

    plt.plot(x,y, label="AE")
    plt.plot(x2,y2, label="LSTM")
    plt.xlabel('False Positive Rate (FPR)')
    plt.ylabel('True Positive Rate (TPR)')
    plt.title('Comparação da ROC entre os dois modelos para o Dataset NSLKDDTest+.')
    plt.legend()
    plt.show()
    plt.savefig('results/fpr_vs_tpr.png')


    df1 = pandas.read_csv("results/MLP_RECALL_vs_PRECISION.csv")
    df2 = pandas.read_csv("results/LSTM_RECALL_vs_PRECISION.csv")

    x = df1.iloc[:,1].values
    y = df1.iloc[:,2].values

    x2 = df2.iloc[:,1].values
    y2 = df2.iloc[:,2].values

    plt.clf()
    plt.plot(x,y, label="AE")
    plt.plot(x2,y2, label="LSTM")
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title('Curva Precision-Recall para o dataset NSLKDDTest+.')
    plt.legend()
    plt.show()
    plt.savefig('results/recall_vs_precision.png')


    df1 = pandas.read_csv("results/mlp_accuracy_train.csv")
    df2 = pandas.read_csv("results/mlp_accuracy_val.csv")

    x = df1.iloc[:,1].values
    y = df1.iloc[:,2].values

    x2 = df2.iloc[:,1].values
    y2 = df2.iloc[:,2].values

    plt.clf()
    plt.plot(x,y, label="train")
    plt.plot(x2,y2, label="val")
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.title('Acurácia do treino da AE_MLP.')
    plt.legend()
    plt.show()
    plt.savefig('results/acc_mlp.png')

    df1 = pandas.read_csv("results/lstm_accuracy_train.csv")
    df2 = pandas.read_csv("results/lstm_accuracy_val.csv")

    x = df1.iloc[:,1].values
    y = df1.iloc[:,2].values

    x2 = df2.iloc[:,1].values
    y2 = df2.iloc[:,2].values

    plt.clf()
    plt.plot(x,y, label="train")
    plt.plot(x2,y2, label="val")
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.title('Acurácia do treino da LSTM.')
    plt.legend()
    plt.show()
    plt.savefig('results/acc_lstm.png')


if __name__=='__main__':
    main()