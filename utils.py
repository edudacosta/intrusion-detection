import torch

def scores(predictions, labels):
    tp = predictions[labels == 1].sum()
    fp = predictions[labels == 0].sum()
    fn = labels[predictions==0].sum()
    tn = (1-labels[predictions==0]).sum()

    p = (predictions == 1).sum()

    if (tp + fp) == 0:
        precision = torch.tensor(0.)
    else:
        precision = tp / (tp + fp)

    if (tp + fn) == 0:
        recall = torch.tensor(0.)
    else:
        recall = tp / (tp + fn)

    if (fp + tn) == 0:
        fpr = torch.tensor(0.)
    else:
        fpr = fp / (fp + tn)
        
    return precision, recall, fpr
