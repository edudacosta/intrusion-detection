# Intrusion Detection

Modelos de machine learning para a detecção de intrusos na rede.

Trabalho da disciplina de Introdução ao Aprendizado de Máquina, do Departamento de Engenharia Elétrica (ENE) da Universidade de Brasília (UnB).

Professor:
    Dr. Daniel Guerreiro e Silva

Estudantes:
    Eduardo da Costa Oliveira, Matrícula: 16/0118417
    Luiz Augusto dos Santos Pires, Matrícula: 16/0134722


# Configuração do ambiente

Instalação do Pytorch:

pip install torch==1.8.1+cpu torchvision==0.9.1+cpu torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html


Instalação das demais dependências:

pip install -r requirements.txt
