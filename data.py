import pandas
import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torchvision.transforms import ToTensor

import numpy as np

import math

def preproc_df(df, cfg, save=False):
    #if save:
        #new_file = os.path.abspath("./") + os.path.split(path)[1].split(".")[0] + "_nostr.txt"

    #attributes = cfg['attributes']
    #for attribute in attributes:
    #    if attributes[attribute] not in ["real","bool"]:
    #        for label in attributes[attribute]:

    # add the column labels
    is_attack = df['class'].map(lambda a: 0 if a == 'normal' else 1)
    df['class'] = is_attack

    #convert to int string data:
    attributes = cfg['attributes']
    df['protocol_type'] = [attributes['protocol_type'].index(ele) for ele in df['protocol_type']]
    df['service'] = [attributes['service'].index(ele) for ele in df['service']]
    df['flag'] = [attributes['flag'].index(ele) for ele in df['flag']]
    
    #print(df.head())
    return df


class IntrusionDataset(Dataset):
    def __init__(self, data_f):
        self.data_frame = data_f

    def __len__(self):
        return len(self.data_frame['class'])

    def __getitem__(self, idx):
        input = self.data_frame.iloc[idx, :-2].values
        label = self.data_frame['class'].iloc[idx]
    
        input = torch.tensor(input)
        label = torch.tensor(label)

        sample = {"input": input.float(), "label": label.float()}
        
        return sample


def normalize(dataframe):
    
    # standarlization
    data_mean = dataframe.mean().to_numpy()
    data_std = dataframe.std().to_numpy()
    data_std[data_std==0] = 1.0
    zm_data = (dataframe - data_mean) / data_std

    # min max normalization
    data_min = zm_data.min().to_numpy()
    data_max = zm_data.max().to_numpy()
    data_diff = data_max - data_min
    data_diff[data_diff==0] = 1.0
    zm_data = zm_data - data_min
    zm_data = zm_data / data_diff

    return zm_data

def get_dataloader(data_frame, batch_size):
    dataset = IntrusionDataset(data_frame)
    return DataLoader(dataset, batch_size=batch_size, shuffle=True)


def split_dataset(df, validation_split=0.0):
    df = df.sample(frac = 1)
    t_split = 1 - validation_split
    normal_samples = df[df["class"] == 0]
    anomaly_samples = df[df["class"] != 0]
    n_normal = normal_samples.shape[0]
    n_anomaly = anomaly_samples.shape[0]

    train_dataframe = pandas.concat((
        normal_samples.iloc[0:math.ceil(t_split*n_normal)],
        anomaly_samples.iloc[0:math.ceil(t_split*n_anomaly)]
    ))
 
    val_dataframe = pandas.concat((
    normal_samples.iloc[math.floor(t_split*n_normal):],
    anomaly_samples.iloc[math.floor(t_split*n_anomaly):]
    ))

    return train_dataframe, val_dataframe